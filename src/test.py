import os
import sys
import nltk
from nltk import word_tokenize
import string


if(len(sys.argv) != 2 ):
	print ("USAGE : python3 test.py testfile ----------------------------------TRY AGAIN")
	exit()

testfile = open(sys.argv[1] , "r+")
finalout = open("final_output", "w+")

count = 0
for line in testfile:

	count+=1
	print(count)

	if(len(line) < 1):
		finalout.write("\n")
		continue

	backup = (line + '.')[:-1]

	
	exclude = set("!.,?")
	line = ''.join(ch for ch in line if ch not in exclude)	
	line = line.split()
	backup = backup.split()

	# i = 0
	# while(i < len(line)):
	# 	if (line[i] == "'re") :
	# 		if( i!=0 and ( line[i-1] == "they" or line[i-1] == "you" or line[i-1] == "They" or line[i-1] == "You") ):
	# 			line[i-1] = line[i-1]+line[i]
	# 			line.pop(i)
	# 			i = i -	1

	# 	elif (line[i] == "'s" ):
	# 		if( i !=0 and (line[i-1] == "it" or line[i-1] == "It")):
	# 			line[i-1] = line[i-1]+line[i]
	# 			line.pop(i)
	# 			i = i-1
	# 	i+=1


	line = nltk.pos_tag(line)
	if(line == []):
		finalout.write("\n")
		continue

	x , y = zip(*line)
	untagged = list(x)

	indices = [i for i, x in enumerate(untagged) if x == "its" or x=="it's" or x =="Its" or x=="It's"]
	for location in indices : 
		length = len(untagged)
		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = untagged[location-1]
				f = line[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		else:
			a = untagged[location-2]
			e = line[location-2][1]
			b = untagged[location-1]
			f = line[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		f1 = open("tempin","w+")
		f1.write(record)
		f1.close()

		process = os.popen("./megam.opt -predict weights_its_it_s multitron tempin")
		output = process.read()
		process.close()
		# os.system("./megam.opt -predict weights_its_it_s multitron tempin > tempout")
		# output = f2.read()
		
		res = int(output.split()[0])
		
		
		if ((untagged[location] == "its" or untagged[location] == "Its" ) and res == 1 ) :
			if(untagged[location] == "its"):
				backup[location] = backup[location].replace("its","it's")
			elif(untagged[location] == "Its"):
				backup[location] = backup[location].replace("Its","It's")
		elif((untagged[location] == "it's" or untagged[location] == "It's" ) and res == 0 ) :
			if(untagged[location] == "it's"):
				backup[location] = backup[location].replace("it's","its")
			elif(untagged[location] == "It's"):
				backup[location] = backup[location].replace("It's","Its")
		

	indices = [i for i, x in enumerate(untagged) if x == "your" or x=="you're" or x =="Your" or x=="You're"]
	for location in indices : 
		length = len(untagged)
		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = untagged[location-1]
				f = line[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		else:
			a = untagged[location-2]
			e = line[location-2][1]
			b = untagged[location-1]
			f = line[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		f1 = open("tempin","w+")
		f1.write(record)
		f1.close()

		process = os.popen("./megam.opt -predict weights_your_you_re multitron tempin")
		output = process.read()
		process.close()


		res = int(output.split()[0])
		
		if ((untagged[location] == "your" or untagged[location] == "Your" ) and res == 1 ) :
			if(untagged[location] == "your"):
				backup[location] = backup[location].replace("your","you're")
			elif(untagged[location] == "Your"):
				backup[location] = backup[location].replace("Your","You're")
		elif((untagged[location] == "you're" or untagged[location] == "You're" ) and res == 0 ) :
			if(untagged[location] == "you're"):
				backup[location] = backup[location].replace("you're","your")
			elif(untagged[location] == "You're"):
				backup[location] = backup[location].replace("You're","Your")
		
	indices = [i for i, x in enumerate(untagged) if x == "their" or x=="they're" or x =="Their" or x=="They're"]
	for location in indices : 
		length = len(untagged)
		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = untagged[location-1]
				f = line[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		else:
			a = untagged[location-2]
			e = line[location-2][1]
			b = untagged[location-1]
			f = line[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		f1 = open("tempin","w+")
		f1.write(record)
		f1.close()

		process = os.popen("./megam.opt -predict weights_their_they_re multitron tempin")
		output = process.read()
		process.close()
		
		res = int(output.split()[0])
		
		if ((untagged[location] == "their" or untagged[location] == "Their" ) and res == 1 ) :
			if(untagged[location] == "their"):
				backup[location] = backup[location].replace("their","they're")
			elif(untagged[location] == "Their"):
				backup[location] = backup[location].replace("Their","They're")
		elif((untagged[location] == "they're" or untagged[location] == "They're" ) and res == 0 ) :
			if(untagged[location] == "they're"):
				backup[location] = backup[location].replace("they're","their")
			elif(untagged[location] == "They're"):
				backup[location] = backup[location].replace("They're","Their")
		

	indices = [i for i, x in enumerate(untagged) if x == "loose" or x=="lose" or x =="Loose" or x=="Lose"]
	for location in indices : 
		length = len(untagged)
		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = untagged[location-1]
				f = line[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		else:
			a = untagged[location-2]
			e = line[location-2][1]
			b = untagged[location-1]
			f = line[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		f1 = open("tempin","w+")
		f1.write(record)
		f1.close()

		process = os.popen("./megam.opt -predict weights_loose_lose multitron tempin")
		output= process.read()
		process.close()
		
		
		res = int(output.split()[0])
		
		if ((untagged[location] == "loose" or untagged[location] == "Loose" ) and res == 1 ) :
			if(untagged[location] == "loose"):
				backup[location] = backup[location].replace("loose","lose")
			elif(untagged[location] == "Loose"):
				backup[location] = backup[location].replace("Loose","Lose")
		elif((untagged[location] == "lose" or untagged[location] == "Lose" ) and res == 0 ) :
			if(untagged[location] == "lose"):
				backup[location] = backup[location].replace("lose","loose")
			elif(untagged[location] == "Lose"):
				backup[location] = backup[location].replace("Lose","Loose")
		

	indices = [i for i, x in enumerate(untagged) if x == "to" or x=="too" or x =="To" or x=="Too"]
	for location in indices : 
		length = len(untagged)
		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = untagged[location-1]
				f = line[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				b = untagged[location-1]
				f = line[location-1][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		else:
			a = untagged[location-2]
			e = line[location-2][1]
			b = untagged[location-1]
			f = line[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				
			elif(length - location == 2):
				d = 0 
				h = 0
				c = untagged[location+1]
				g = line[location+1][1]
				
			else:
				d = untagged[location+2]
				h = line[location+2][1]
				c = untagged[location+1]
				g = line[location+1][1]
				
		record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		f1 = open("tempin","w+")
		f1.write(record)
		f1.close()

		process = os.popen("./megam.opt -predict weights_to_too multitron tempin")
		output = process.read()
		process.close()

		res = int(output.split()[0])
		
		if ((untagged[location] == "to" or untagged[location] == "To" ) and res == 1 ) :
			if(untagged[location] == "to"):
				backup[location] = backup[location].replace("to","too")
			elif(untagged[location] == "To"):
				backup[location] = backup[location].replace("To","Too")
		elif((untagged[location] == "too" or untagged[location] == "Too" ) and res == 0 ) :
			if(untagged[location] == "too"):
				backup[location] = backup[location].replace("too","to")
			elif(untagged[location] == "Too"):
				backup[location] = backup[location].replace("Too","To")
		
		

	finalout.write(' '.join(map(str, backup)) + "\n")

testfile.close()
finalout.close()