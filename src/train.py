import sys
import os
import json
import random
import nltk
from nltk.corpus import brown
from nltk.corpus import reuters
from nltk.corpus import treebank
from nltk.corpus import inaugural
from nltk.corpus import gutenberg

dictofcorpora = {}
dictofcorpora["its/it's"] = []
dictofcorpora["your/you're"] = []
dictofcorpora["their/they're"] = []
dictofcorpora["loose/lose"] = []
dictofcorpora["to/too"] = []

untagged_corpus = reuters.sents() + inaugural.sents() + gutenberg.sents()
tagged_corpus = []

count = 0
for sentence in untagged_corpus :
	if( "its" in sentence or "Its" in sentence or "it's" in sentence or "It's" in sentence or "your" in sentence or "you're" in sentence or "Your" in sentence or "You're" in sentence or "their" in sentence or "they're" in sentence or "Their" in sentence or "They're" in sentence or "loose" in sentence or "lose" in sentence or "Loose" in sentence or "Lose" in sentence or "to" in sentence or "too" in sentence or "To" in sentence or "Too" in sentence):
		tagged_sentence = nltk.pos_tag(sentence)
		tagged_corpus.append(tagged_sentence)
		print(count)
		count+=1

final_corpus = brown.tagged_sents() + treebank.tagged_sents() + tagged_corpus 
ulti_corpus = open("Corpus_final","w+")
for sent in final_corpus:
	ulti_corpus.write(str(sent) + "\n")
ulti_corpus.close();


for k in final_corpus:
	x , y = zip(*k)
	sent = list(x)
	record = ""

	if "its" in sent:

		location = sent.index("its")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			

		dictofcorpora["its/it's"].append(record)	
	if ( "it" in sent and "is" in sent and (sent.index("is") - sent.index("it"))==1):
		
		location = sent.index("it")
		location2 = sent.index("is")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location2 == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			

		dictofcorpora["its/it's"].append(record)	
		

		#dictofcorpora["its"].append(k)
	if ( "it's" in sent ):

		location = sent.index("it's")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		
			
		dictofcorpora["its/it's"].append(record)	

	if ("your") in sent:
		location = sent.index("your")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		
		dictofcorpora["your/you're"].append(record)
	if ("you're" in sent):
		location = sent.index("you're")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		dictofcorpora["your/you're"].append(record)	
	if ( "you" in sent and "are" in sent and (sent.index("are") - sent.index("you"))==1) :
		
		location = sent.index("you")
		location2 = sent.index("are")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location2 == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			

		dictofcorpora["your/you're"].append(record)	
		
	if ("their") in sent:
		location = sent.index("their")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)			
		dictofcorpora["their/they're"].append(record)	
	if ("they're" in sent):
		location = sent.index("they're")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["their/they're"].append(record)	
	if ( "they" in sent and "are" in sent and (sent.index("are") - sent.index("they"))==1) :
		
		location = sent.index("they")
		location2 = sent.index("are")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location2 == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location2 == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location2 == 2):
				d = 0 
				h = 0
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location2+2]
				h = k[location2+2][1]
				c = sent[location2+1]
				g = k[location2+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["their/they're"].append(record)	
		
	if ("loose") in sent:
		location = sent.index("loose")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["loose/lose"].append(record)	
	if ("lose") in sent:
		location = sent.index("lose")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["loose/lose"].append(record)	

	if ("to") in sent:
		location = sent.index("to")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "0"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["to/too"].append(record)	
	if ("too") in sent:
		location = sent.index("too")
		length = len(sent)

		if(location == 0):
			a = 0
			b = 0
			e = 0
			f = 0
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)

		elif(location == 1):
			a = 0
			e = 0
			if(length - location == 1):
				b = sent[location-1]
				f = k[location-1][1]
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				b = sent[location-1]
				f = k[location-1][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
		else:
			a = sent[location-2]
			e = k[location-2][1]
			b = sent[location-1]
			f = k[location-1][1]
			if(length - location == 1):
				c = 0
				d = 0
				g = 0
				h = 0
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			elif(length - location == 2):
				d = 0 
				h = 0
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			else:
				d = sent[location+2]
				h = k[location+2][1]
				c = sent[location+1]
				g = k[location+1][1]
				record = "1"+" a_"+str(a)+" b_"+str(b)+" c_"+str(c)+" d_"+str(d)+" e_"+str(e)+" f_"+str(f)+" g_"+str(g)+" h_"+str(h)
			
		dictofcorpora["to/too"].append(record)	


for ids in dictofcorpora:
	filename = ids.replace('/','_')
	filename = filename.replace('\'' ,'_') 
	f1 = open( filename , "w+")
	for record in dictofcorpora[ids]:
		f1.write(record + "\n")
	f1.close()
	print(str(ids) + " " + str(len(dictofcorpora[ids])) + " " + dictofcorpora[ids][0])

for ids in dictofcorpora:
	filename = ids.replace('/','_')
	filename = filename.replace('\'' ,'_')
	os.system("./megam.opt multitron "+filename+" > weights_"+filename)


			
	

